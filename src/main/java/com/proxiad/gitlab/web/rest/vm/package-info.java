/**
 * View Models used by Spring MVC REST controllers.
 */
package com.proxiad.gitlab.web.rest.vm;
